/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.com/docs/node-apis/
 */

// You can delete this file if you're not using it
//
const path = require("path")
const { createFilePath } = require("gatsby-source-filesystem")

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions

  if (node.internal.type === "Mdx") {
    const value = createFilePath({ node, getNode })

    const name = getNode(node.parent).sourceInstanceName

    if (name === "blog") {
      createNodeField({
        name: "slug",
        node,
        value: `/blog${value}`,
      })
    } else if (name === "pages") {
      createNodeField({
        name: "slug",
        node,
        value,
      })
    }
  }
}

exports.createPages = async ({ graphql, actions, reporter }) => {
  const { createPage } = actions

  const result = await graphql(`
    query {
      allMdx {
        edges {
          node {
            id
            fields {
              slug
            }
            frontmatter {
              draft
            }
          }
        }
      }
    }
  `)

  if (result.errors) {
    reporter.panicOnBuild('🚨  ERROR: Loading "createPages" query')
  }

  const posts = result.data.allMdx.edges.filter(
    ({ node }) => !node.frontmatter.draft
  )

  const component = path.resolve("./src/components/layout.js")
  const blogComponent = path.resolve("./src/components/blog-layout.js")

  posts.forEach(({ node }, index) => {
    const isBlog = node.fields.slug.substring(1, 5) === "blog"
    createPage({
      path: node.fields.slug,
      component: isBlog ? blogComponent : component,
      context: { id: node.id },
    })
  })
}
