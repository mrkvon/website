<div class="center">
<p id="main-text">mrkvon is a nickname of a human</p>

<p>(they/them)</p>

<!--[<i class="icon-git" />](https://git.mrkvon.org/mrkvon)-->
[<i class="icon-gh" />](https://github.com/mrkvon)

---

&#x6D;&#x72;&#x6B;&#x76;&#x6F;&#x6E;&#x20;&#x61;&#x74;&#x20;&#x70;&#x72;&#x6F;&#x74;&#x6F;&#x6E;&#x6D;&#x61;&#x69;&#x6C;&#x20;&#x64;&#x6F;&#x74;&#x20;&#x63;&#x6F;&#x6D;

[PGP key](/files/public.key)

</div>
