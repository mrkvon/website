## active

- [SolidCouch](https://solidcouch.org) - decentralized hospitality exchange
- [Tired Bike](https://tired.bike) - SolidCouch instance for touring cyclists and other slow travellers
- [tady.app](https://tady.app) - nostr app for local news

## incubated, paused

- [mistoskoly.cz](https://mistoskoly.cz)
- [math livegraph](https://math.livegraph.org)
- [ditup](https://ditup.org)
- [Trustroots](https://trustroots.org) (contributor)

## past

- [osm2pdf](https://www.npmjs.com/package/osm2pdf)
- [influenced](https://influenced.livegraph.org)
- [nomadbase in Lisbon](https://lisbon.nomadbase.org) (winter 2015 - 2016)
- [comix diary](/projects/comix-diary)
- some graphics for [MultiRacer](https://franticware.com/multiracer)
