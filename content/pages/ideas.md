## Keywords

collaboration, do-it-together, freedom, community, self-directed education

## Interesting books

### 2023

- Jennifer Cook O'Toole: Autism in Heels: The Untold Story of a Female Life on the Spectrum
- Jenara Nerenberg: Divergent Mind: Thriving in a World That Wasn't Designed for You
- Diana Leafe Christian: Creating a Life Together: Practical Tools to Grow Ecovillages and Intentional Communities
- Grace Llewellyn: The Teenage Liberation Handbook: How to Quit School and Get a Real Life and Education
- Silvia Federici: Caliban and the Witch: Women, the Body and Primitive Accumulation
- Caroline Criado Perez: Invisible Women: Exposing Data Bias in a World Designed for Men

### 2012

- Tom Hodgkinson: Freedom Manifesto
- Stephen Wolfram: A New Kind of Science
- Nassim Nicholas Taleb: Antifragile: Things That Gain from Disorder
- Howard Bloom: Global Brain: The Evolution of Mass Mind from the Big Bang to the 21st Century
- James Surowiecki: The Wisdom of Crowds
- Antoine de Saint-Exupéry: Citadelle
- Robert B. Cialdini: Influence: Science and Practice
- Andrew M. Lobaczewski: Political Ponerology (interesting book on psychopathy and politics)
- Eckhart Tolle: The Power of Now: A Guide to Spiritual Enlightenment
