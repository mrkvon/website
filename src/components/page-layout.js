/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.com/docs/use-static-query/
 */

import React from "react"
import PropTypes from "prop-types"
import Header from "./header"
import SEO from "./seo"
import "./layout.css"

const Layout = ({ children, location }) => {
  return (
    <>
      <SEO />
      <div id="wrapper">
        <Header location={location} />
        <main id="page">{children}</main>
      </div>
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
